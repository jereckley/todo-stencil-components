import {Component, Prop} from '@stencil/core';

export interface ToDos {
  done:boolean;
  description:string;
}

@Component({
  tag: 'to-do-body',
  styleUrl: 'to-do-body.scss',
  shadow: true
})
export class ToDoBody {
  @Prop() listAwesome: Array<ToDos>;

  render() {
    return (
      <div>
        <to-do-lists list-title={'To Do'}>
          {this.listAwesome ? this.listAwesome.map((todo: ToDos, index: number) => {
              return todo.done ? null : <to-do-list-item toDoItem={todo} itemIndex={index}></to-do-list-item>
            }
          ) : null}
        </to-do-lists>
        <to-do-lists list-title={'Done'}>
          {this.listAwesome ? this.listAwesome.map((todo: ToDos, index: number) =>{
              return todo.done ? <to-do-list-item toDoItem={todo} itemIndex={index}></to-do-list-item> : null
            }
          ): null}
        </to-do-lists>
      </div>
    );
  }
}
