import { Component } from '@stencil/core';
import { Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'to-do-input',
  styleUrl: 'to-do-input.scss',
  shadow: true
})
export class ToDoInput {
  @Event() toDoInputTextUpdated: EventEmitter;

  textChange(event) {
    this.toDoInputTextUpdated.emit(event.key)
  }

  render() {
    return (
      <div class={'input-div'}>
        <input type={'text'} onKeyDown={this.textChange.bind(this)}></input>
      </div>
    );
  }
}
