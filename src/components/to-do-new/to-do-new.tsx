import {Component, State, Event, EventEmitter} from '@stencil/core';
import { Listen } from '@stencil/core';

@Component({
  tag: 'to-do-new',
  styleUrl: 'to-do-new.scss',
  shadow: true
})
export class ToDoNew {
  @State() toDoText: string = '';
  @Event() addToDoEvent: EventEmitter;

  @Listen('toDoInputTextUpdated')
  toDoInputTextUpdatedHandler(event) {
    if(event.detail.toLowerCase() === 'backspace') {
      if(this.toDoText.length > 0) this.toDoText = this.toDoText.slice(0, -1);
    }else if(event.detail.toLowerCase() === 'enter') {
      if(this.toDoText.length > 0) this.addToDoEvent.emit( this.toDoText )
    }else {
      this.toDoText += event.detail;
    }
  }

  addToDo() {
    if(this.toDoText.length > 0) this.addToDoEvent.emit( this.toDoText )
  }

  render() {
    return (
      <div class={'new-div'}>
        <to-do-input></to-do-input>
        <to-do-button onClick={this.addToDo.bind(this)}>add to do</to-do-button>
      </div>
    );
  }
}
