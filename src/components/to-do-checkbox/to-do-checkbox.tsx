import {Component, Prop, Event, EventEmitter} from '@stencil/core';

@Component({
  tag: 'to-do-checkbox',
  styleUrl: 'to-do-checkbox.scss',
  shadow: true
})
export class ToDoCheckbox {
  @Prop() isChecked: boolean;
  @Event() checkboxToggled: EventEmitter;

  checkboxChanged() {
    this.checkboxToggled.emit()
  }

  render() {
    return (
      <div class={'checkbox-div'}>
        <input type={'checkbox'} onChange={this.checkboxChanged.bind(this)} checked={this.isChecked}/>
      </div>
    );
  }
}
