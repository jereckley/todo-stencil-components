import {Component, Prop} from '@stencil/core';

@Component({
  tag: 'to-do-button',
  styleUrl: 'to-do-button.scss',
  shadow: true
})
export class ToDoButton {
  @Prop() isRed;

  render() {
    return (
      <div class={'button-container ' + (this.isRed ? 'button-red' : '')}>
        <div class={'button-text'}>
          <slot></slot>
        </div>
      </div>
    );
  }
}
