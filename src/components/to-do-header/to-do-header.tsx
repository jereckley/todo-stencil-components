import { Component } from '@stencil/core';

@Component({
  tag: 'to-do-header',
  styleUrl: 'to-do-header.scss',
  shadow: true
})
export class ToDoHeader {

  render() {
    return (
      <div>
        <to-do-new></to-do-new>
      </div>
    );
  }
}
