import {Component, Listen, State} from '@stencil/core';
import {ToDos} from "../to-do-body/to-do-body";

@Component({
  tag: 'to-do',
  styleUrl: 'to-do.scss',
  shadow: true
})
export class ToDo {
  @State() list: Array<ToDos> = [
    {
      done: false,
      description: "mow the lawn"
    },
    {
      done: false,
      description: "feed the dog"
    },
    {
      done: true,
      description: "eat food"
    }
  ];


  @Listen('addToDoEvent')
  addToDoEventHandler(event: CustomEvent) {
    this.list = [...this.list, { done: false, description: event.detail}];
  }

  @Listen('removeItemAtIndex')
  removeItemAtIndexHandler(event: CustomEvent) {
    this.list.splice(event.detail, 1);
    this.list = [...this.list]
  }

  @Listen('toggleItemAtIndex')
  toggleItemAtIndexHandler(event: CustomEvent){
    console.log(event.detail);
    this.list[event.detail].done = !this.list[event.detail].done;
    this.list.map( item => {return {...item}});
    this.list = [...this.list];
  }

  render() {
    return (
      <div class={'happy'}>
        <to-do-header></to-do-header>
        <to-do-body listAwesome={this.list}></to-do-body>
      </div>
    );
  }
}
