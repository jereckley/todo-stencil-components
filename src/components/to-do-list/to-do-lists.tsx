import {Component, Prop} from '@stencil/core';

@Component({
  tag: 'to-do-lists',
  styleUrl: 'to-do-lists.scss',
  shadow: true
})
export class ToDoLists {
  @Prop() listTitle;

  render() {
    return (
      <div class={'list-containing-div'}>
        <div class={'title-div'}>{this.listTitle}:</div>
        <div class={'list-div'}>
          <slot></slot>
        </div>
      </div>
    );
  }
}
