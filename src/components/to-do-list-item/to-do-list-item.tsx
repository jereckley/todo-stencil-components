import {Component, Prop, Event, EventEmitter, Listen} from '@stencil/core';
import {ToDos} from "../to-do-body/to-do-body";

@Component({
  tag: 'to-do-list-item',
  styleUrl: 'to-do-list-item.scss',
  shadow: true
})
export class ToDoListItem {
  @Prop() toDoItem:ToDos;
  @Prop() itemIndex: number;
  @Event() removeItemAtIndex: EventEmitter;
  @Event() toggleItemAtIndex: EventEmitter;
  @Listen('checkboxToggled')
  toggled() {
    this.toggleItemAtIndex.emit(this.itemIndex)
  }

  remove() {
    this.removeItemAtIndex.emit(this.itemIndex)
  }

  render() {
    return (
      <div class={'list-item-containing-div'}>
        <to-do-checkbox isChecked={this.toDoItem.done}></to-do-checkbox>
        <div class={'item-text'}>{this.toDoItem.description}</div>
        <to-do-button isRed={true} onClick={this.remove.bind(this)}>delete</to-do-button>
      </div>
    );
  }
}
