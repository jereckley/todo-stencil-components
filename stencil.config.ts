import { Config } from '@stencil/core';
import {sass} from '@stencil/sass';

export const config: Config = {
  namespace: 'to-do'
  outputTargets:[
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: null,
      baseUrl: '/todostencil'
    }
  ],
  plugins: [
    sass()
  ]
};
